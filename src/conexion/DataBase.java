package conexion;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataBase
{
    static Statement statement = null;
    static Connection conexion = null;
    private static Statement MyStatement;
    public static long tiempoInicio;
    public static long tiempoEjecucion;

    public static Connection conectar()
    {
        String driver = "org.mysql.JDBC";
        String URLsakila = "jdbc:mysql://localhost:3306/sakila";
        String URLworld = "jdbc:mysql://localhost:3306/world";

        String username = "root";
        String password = "CCSS2000";

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conexion = DriverManager.getConnection(URLsakila,username, password);
            MyStatement = conexion.createStatement();

            if(conexion != null)
            {
                System.out.println("Conectando a la Base de datos [" + conexion + "] Ok");
            }

        }catch (ClassNotFoundException | SQLException e)
        {
            System.out.println("Error:" + e);
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, e);
        }
        return conexion;
    }

    public static void consultas() {
        try{
            Connection con = conectar();

            //1ª ejecución 1ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res1a = MyStatement.executeQuery("SELECT COUNT(rating) AS Contador, rating FROM film WHERE rating LIKE 'PG';");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 1: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 1ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res1b = MyStatement.executeQuery("SELECT COUNT(rating) AS Contador, rating FROM film WHERE rating LIKE 'PG';");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 1: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 2ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res2a = MyStatement.executeQuery("SELECT MIN(amount) FROM payment;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 2: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 2ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res2b = MyStatement.executeQuery("SELECT MIN(amount) FROM payment;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 2: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 3ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res3a = MyStatement.executeQuery("SELECT *FROM rental;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 3: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 3ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res3b = MyStatement.executeQuery("SELECT *FROM rental;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 3: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 4ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res4a = MyStatement.executeQuery("SELECT first_name FROM actor ORDER BY last_update;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 4: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 4ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res4b = MyStatement.executeQuery("SELECT first_name FROM actor ORDER BY last_update;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 4: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 5ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res5a = MyStatement.executeQuery("SELECT category, total_sales FROM sales_by_film_category WHERE total_sales > (SELECT AVG(total_sales) FROM sales_by_film_category);");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 5: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 5ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res5b = MyStatement.executeQuery("SELECT category, total_sales FROM sales_by_film_category WHERE total_sales > (SELECT AVG(total_sales) FROM sales_by_film_category);");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 5: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 6ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res6a = MyStatement.executeQuery("SELECT * FROM actor CROSS JOIN sales_by_film_category;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 6: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 6ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res6b = MyStatement.executeQuery("SELECT * FROM actor CROSS JOIN sales_by_film_category;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 6: " + tiempoEjecucion + " nano segundos.");

            //Cambio a base de datos world.
            tiempoInicio = System.nanoTime();
            MyStatement.executeQuery("USE world;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de ejecución, cambio de base de datos: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 7ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res7a = MyStatement.executeQuery("SELECT CountryCode, AVG(Population) FROM city GROUP BY CountryCode;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 7: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 7ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res7b = MyStatement.executeQuery("SELECT CountryCode, AVG(Population) FROM city GROUP BY CountryCode;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 7: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 8ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res8a = MyStatement.executeQuery("SELECT MAX(Population) FROM country;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 8: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 8ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res8b = MyStatement.executeQuery("SELECT MAX(Population) FROM country;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 8: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 9ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res9a = MyStatement.executeQuery("SELECT * FROM countrylanguage;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 9: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 9ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res9b = MyStatement.executeQuery("SELECT * FROM countrylanguage;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 9: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecucion 10ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res10a = MyStatement.executeQuery("SELECT DISTINCT Percentage FROM countrylanguage;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 10: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecucion 10ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res10b = MyStatement.executeQuery("SELECT DISTINCT Percentage FROM countrylanguage;");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 10: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 11ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res11a = MyStatement.executeQuery("SELECT Name FROM city  WHERE Population > (SELECT Population FROM city WHERE Name = 'Breda');");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 11: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 11ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res11b = MyStatement.executeQuery("SELECT Name FROM city  WHERE Population > (SELECT Population FROM city WHERE Name = 'Breda');");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 11: " + tiempoEjecucion + " nano segundos.");

            //1ª ejecución 12ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res12a = MyStatement.executeQuery("SELECT COUNT(*) FROM city INNER JOIN countrylanguage ON city.CountryCode = countrylanguage.CountryCode WHERE Name = 'Paris';");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 1ª ejecución de la consulta 12: " + tiempoEjecucion + " nano segundos.");

            //2ª ejecución 12ª consulta
            tiempoInicio = System.nanoTime();
            ResultSet res12b = MyStatement.executeQuery("SELECT COUNT(*) FROM city INNER JOIN countrylanguage ON city.CountryCode = countrylanguage.CountryCode WHERE Name = 'Paris';");
            tiempoEjecucion = System.nanoTime()-tiempoInicio;
            System.out.println("Tiempo de la 2ª ejecución de la consulta 12: " + tiempoEjecucion + " nano segundos.");

        }catch (Exception e)
        {
            System.out.println("Error: " + e);
            System.out.println("Error cargando el driver.");
        }
    }

    public static void main(String[] args){
        consultas();
    }
}

